package com.project.springwebapp.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.springwebapp.entity.Report;
import com.project.springwebapp.entity.ReportContainer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(StartController.class)
public class StartControllerTest {
    MockMvc mvc;

    @Autowired
    WebApplicationContext context;

    @SpyBean
    ReportContainer container;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }

    @Test
    public void testApi() throws Exception {

        Report report1 = Report.getBuilder().withUser(1).withLevel(1).withResult(55L).build();
        Report report2 = Report.getBuilder().withUser(1).withLevel(2).withResult(8L).build();
        Report report3 = Report.getBuilder().withUser(1).withLevel(3).withResult(15L).build();
        Report report4 = Report.getBuilder().withUser(2).withLevel(3).withResult(22L).build();

        Set<Report> reports = new HashSet<>();
        reports.add(report1);
        reports.add(report2);
        reports.add(report3);
        reports.add(report4);

        for (Report report : reports) {
            MvcResult mvcResult = mvc.perform(
                    MockMvcRequestBuilders.put("/setinfo")
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(mapToJson(report))
            ).andExpect(status().isOk())
                    .andReturn();

            String content = mvcResult.getResponse().getContentAsString();
            assertEquals(content, "Report successfully created!" + report.toString());
            System.out.println(container.getReports());
        }

        MvcResult byLevel = mvc.perform(
                MockMvcRequestBuilders.get("/levelinfo/3")
        ).andExpect(status().isOk()).andReturn();

        Report[] byLevelResponse = mapFromJson(byLevel.getResponse().getContentAsString(), Report[].class);
        assertEquals(byLevelResponse[0].toString(), report4.toString());
        assertEquals(byLevelResponse[1].toString(), report3.toString());
        assertTrue(byLevelResponse.length == 2);


        MvcResult byUser = mvc.perform(
                MockMvcRequestBuilders.get("/userinfo/1")
        ).andExpect(status().isOk()).andReturn();

        Report[] byUserResponse = mapFromJson(byUser.getResponse().getContentAsString(), Report[].class);
        assertEquals(byUserResponse[0].toString(), report1.toString());
        assertEquals(byUserResponse[1].toString(), report3.toString());
        assertEquals(byUserResponse[2].toString(), report2.toString());
        assertTrue(byUserResponse.length == 3);
    }
}
