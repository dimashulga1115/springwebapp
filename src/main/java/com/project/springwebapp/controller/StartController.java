package com.project.springwebapp.controller;

import com.project.springwebapp.entity.Report;
import com.project.springwebapp.entity.ReportContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class StartController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartController.class);


    @Autowired
    ReportContainer reportContainer;

    @Autowired
    public ApplicationContext context;

    @GetMapping("/userinfo/{user_id}")
    public Set<Report> getUserInfo(@PathVariable long user_id){
        Set<Report> userInfo = reportContainer.filterUsers(user_id);
        LOGGER.info(String.format("Reports with level_id=%d : %s", user_id, userInfo.toString()));
        return userInfo;
    }

    @GetMapping("/levelinfo/{level_id}")
    public Set<Report> getLevelInfo(@PathVariable long level_id){
        Set<Report> levelInfo = reportContainer.filterLevels(level_id);
        LOGGER.info(String.format("Reports with level_id=%d : %s", level_id, levelInfo.toString()));
        return levelInfo;
    }

    @PutMapping("/setinfo")
    public String putInfo(@RequestBody Report report){
        reportContainer.putReport(report);
        LOGGER.info(String.format("Report successfully created: %s", report.toString()));
        LOGGER.info(reportContainer.getReports().toString());
        return "Report successfully created!" + report.toString();
    }
}
