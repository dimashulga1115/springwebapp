package com.project.springwebapp.entity;

import com.fasterxml.jackson.annotation.*;

import javax.swing.*;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicLong;

public class Report {
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @JsonProperty("user_id")
    private User user;

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @JsonProperty("level_id")
    private Level level;

    @JsonProperty
    private Long result;

    private Report(){}

    private Report(Report report){
        this.user = report.user;
        this.level = report.level;
        this.result = report.result;
    }

    @Override
    public String toString() {
        return "Report{" +
                "user=" + user +
                ", level=" + level +
                ", result=" + result +
                '}';
    }

    public User getUser() {
        return user;
    }

    public Level getLevel() {
        return level;
    }

    public Long getResult() {
        return result;
    }

    @JsonProperty("user_id")
    private synchronized void setUserById(long userId) {
        this.user = User.fromId(userId);
    }

    @JsonProperty("level_id")
    private synchronized void setLevelById(long levelId) {
        this.level = Level.fromId(levelId);
    }

    private static final Comparator<Report> byResult = Comparator.comparingLong(Report::getResult).reversed();


    public static Comparator<Report> byResult(){
        return byResult;
    }

    public static Builder getBuilder(){
        return new Report().new Builder();
    }

    public class Builder{
        private Builder() {}

        public Builder withUser(long userId){
            Report.this.user = User.fromId(userId);
            return this;
        }

        public Builder withUser(User user){
            Report.this.user = user;
            return this;
        }

        public Builder withLevel(long levelId){
            Report.this.level = Level.fromId(levelId);
            return this;
        }

        public Builder withLevel(Level level){
            Report.this.level = level;
            return this;
        }

        public Builder withResult(Long result){
            Report.this.result = result;
            return this;
        }

        public Report build(){
            return new Report(Report.this);
        }
    }
}
