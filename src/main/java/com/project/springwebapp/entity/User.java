package com.project.springwebapp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = User.class)
public class User {
    @JsonProperty
    long id;

    public User() {}

    private User(long id) {
        this.id = id;
    }

    public static User fromId(long id){
        return new User(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                '}';
    }
}
