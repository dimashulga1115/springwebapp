package com.project.springwebapp.entity;

import org.springframework.stereotype.Component;

import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

@Component
public class ReportContainer {
    private final NavigableSet<Report> reports;

    public ReportContainer() {
        this.reports = new ConcurrentSkipListSet<>(Report.byResult());
    }

    public synchronized void putReport(Report report){
        reports.add(report);
        if(reports.size() > 20) reports.pollLast();
    }

    public synchronized Set<Report> filterUsers(long userId){
        return reports.stream().filter(report -> report.getUser().id == userId)
                .collect(Collectors.toCollection(()->new TreeSet<>(Report.byResult())));
    }

    public synchronized Set<Report> filterLevels(long levelId){
        return reports.stream().filter(report -> report.getLevel().id == levelId)
                .collect(Collectors.toCollection(()->new TreeSet<>(Report.byResult())));
    }

    public synchronized Set<Report> getReports(){
        return reports;
    }
}
