package com.project.springwebapp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = Level.class)
public class Level {
    @JsonProperty
    long id;

    public Level() {}

    private Level(long id) {
        this.id = id;
    }
    public static Level fromId(long id){
        return new Level(id);
    }

    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                '}';
    }
}
